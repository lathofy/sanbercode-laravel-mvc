<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
<h1>Buat Account Baru</h1>
	<h3>Sign Up Form</h3>
	<form action="/welcome_post" method="POST">
    @csrf
    
		<!--Nama-->
		<label>First Name: </label><br><br>
		<input type="text" name="nama_depan"><br><br>
		<label>Last Name: </label><br><br>
		<input type="text" name="nama_belakang"><br><br>

		<!--Jenis Kelamin-->
		<label>Gender: </label><br><br>
		<input type="radio" name="jns_kelamin" value="laki">Male <br>
		<input type="radio" name="jns_kelamin" value="perempuan">Female <br>
		<input type="radio" name="jns_kelamin" value="lain">Other <br><br>

		<!--Lain-->
		<label>Nationality: </label><br><br>
		<select>
			<option value="ind">Indonesian</option>
			<option value="sgp">Singaporean</option>
			<option value="my">Malaysian</option>
			<option value="vn">Vietnamese</option>
			<option value="usa">American</option>
		</select><br><br>

		<label>Language Spoken: </label><br><br>
		<input type="checkbox" name="bahasa" value="bahasa">Bahasa Indonesia <br>
		<input type="checkbox" name="bahasa" value="inggris">English <br>
		<input type="checkbox" name="bahasa" value="lain"> Other <br><br>

		<label>Bio: </label><br><br>
		<textarea cols="40" rows="10"></textarea>
		<br>
		<input type="submit" value="Sign Up">
	</form>
</body>
</html>